//Utilizando o AJAX -> principal forma de consumir informações do servidor pelo JavaScript
var xhr = new XMLHttpRequest(); //classe responsável para recuperar informações do servidor

xhr.open('GET', 'https://api.github.com/users/rodrigodabreu')
xhr.send(null); //enviando os parametros da requisição

//como saber se a requisição terminou

//requisiçãi assync -> ela não acontece no mesmo momento da ação
xhr.onreadystatechange = function() {
    if(xhr.readyState === 4){
        console.log(JSON.parse(xhr.responseText));
    }
}

//promises -> promessas são códigos que não influenciam nas funções do javascript
var minhaPromise = function(){
    return new Promise(function(resolve, reject){
        var xhr = new XMLHttpRequest();
        xhr.open('GET','https://api.gi2thub.com/users/diego3g');
        xhr.send(null);

        xhr.onreadystatechange = function(){
            if(xhr.readyState === 4){
                if(xhr.status === 200){
                    resolve(JSON.parse(xhr.responseText))
                }else {
                    reject('ERRO - erro na requisição');
                }
            }
        }
    });
}

minhaPromise()
.then(function(response) {
    console.log(response);
})
.catch(function(error){
    console.warn(error);
});