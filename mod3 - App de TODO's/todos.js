//Referenciando os elementos
var todoList = document.querySelector('#app ul');
var inputElement = document.querySelector('#app input');
var buttonElement = document.querySelector('#app button');

// buscando uma lista de todos que está armazenada no localstorage do navegador e quando não existir cria a lista de todos
var todos =JSON.parse(localStorage.getItem('list_todos'))||[];

function renderTodos(){
    todoList.innerHTML = "";
    for(todo of todos){
        var todoElement = document.createElement('li');
        var todoText = document.createTextNode(todo);

        var linkElement = document.createElement('a');
        linkElement.setAttribute('href', '#');
        var linkText = document.createTextNode('Excluir');

        linkElement.appendChild(linkText);
        
        var pos = todos.indexOf(todo);
        linkElement.setAttribute('onclick', 'deleteTodo('+pos+')');

        todoElement.appendChild(todoText);
        todoElement.appendChild(linkElement);
        todoList.appendChild(todoElement);
    }
}
renderTodos();

function addTodo(){
     var todoText = inputElement.value;
     todos.push(todoText);
     console.log(todos);
     inputElement.value = "";
     renderTodos();
     saveToStorage();
}

buttonElement.onclick = addTodo;

function deleteTodo(pos){
    todos.splice(pos, 1);
    renderTodos();
    saveToStorage();
}

//salvando no storage do navegador
function saveToStorage(){
    //JSON -> JavaScript Object Notation
    localStorage.setItem('list_todos', JSON.stringify(todos));
}