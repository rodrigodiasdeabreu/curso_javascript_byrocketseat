//referenciando os elementos do html
var ulElement = document.querySelector('#app ul');
var inputElement = document.querySelector('#app input');
var buttonElement = document.querySelector('#app button');

//Lista de usuários
var namesList = [
    "Diego",
    "Gabriel",
    "Lucas",
    "Vivian"
];

function renderNames() {
    ulElement.innerHTML = ""; //limpa a lista antes de renderizar
    for (name of namesList) {
        var nameLi = document.createElement('li');
        var nameText = document.createTextNode(name);
        nameLi.appendChild(nameText); //atribuindo o valor de name na li
        ulElement.appendChild(nameLi); //atribuindo a li n
    }
}


//adicionando nome na lista
function addName() {
    var newName = inputElement.value; //pegando o valor do input
    namesList.push(newName); //adicionando no array
    inputElement.value = ""; //limpando o campo de input
    renderNames();
}

renderNames(); //chamando a função para mostrar a lista de nomes
buttonElement.onclick = addName;