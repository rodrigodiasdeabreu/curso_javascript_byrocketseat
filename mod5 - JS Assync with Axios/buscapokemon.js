function buscaPokemon() {

    var inputElement = document.querySelector('#app input');
    var idPokemon = inputElement.value
    var baseURL = 'https://pokeapi.co/api/v2/pokemon/';

    var divUlElement = document.getElementById('lista');
    var ulElement = document.createElement('ul');

    //limpa o campo de inputText
    inputElement.value = "";

    $('ul').empty(); //https://stackoverflow.com/questions/7004059/jquery-remove-all-list-items-from-an-unordered-list

    axios.get(baseURL + idPokemon)
        .then(response => {

            //TODO
            //cria um novo elemento li
            //adiciona o texto vindo da response nessa li
            //append li na ul

            //tratando o nome
            var nome = response.data.name;
            //criando uma tag li
            var nomeElement = document.createElement('li');
            //atribuindo o texto digitado ao texto da li
            var nomeText = document.createTextNode(nome);
            //vinculando o texto a li
            nomeElement.appendChild(nomeText);
            //vinculando a li a ul
            ulElement.appendChild(nomeElement);

            divUlElement.appendChild(ulElement);

            //tratando o height
            var height = response.data.height;
            var heightElement = document.createElement('li');
            var heightText = document.createTextNode(height);

            heightElement.appendChild(heightText);
            ulElement.appendChild(heightElement);
            divUlElement.appendChild(ulElement);

            //tratando o weight
            var weight = response.data.weight;
            var weightElement = document.createElement('li');
            var weightText = document.createTextNode(weight);

            weightElement.appendChild(weightText);
            ulElement.appendChild(weightElement);
            divUlElement.appendChild(ulElement);

            //tratando a espécie 
            var species = response.data.species.name;
            var speciesElement = document.createElement('li');
            var speciesText = document.createTextNode(species);

            speciesElement.appendChild(speciesText);
            ulElement.appendChild(speciesElement);
            divUlElement.appendChild(ulElement);

        })
        .catch(error => console.log(error));
}